export const  record = [
  {
    id: 122,
    text: "Waiting",
    progressvalue: 20,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  },
  {
    id: 123,
    text: "In progress",
    progressvalue: 80,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  },
  {
    id: 124,
    text: "Upcoming",
    progressvalue: 50,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  },
  {
    id: 125,
    text: "Finished",
    progressvalue: 120,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  },
  {
    id: 126,
    text: "Waiting",
    progressvalue: 20,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  }
];

