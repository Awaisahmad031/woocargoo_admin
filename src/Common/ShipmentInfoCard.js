import React, { Component } from 'react'
import { connect } from 'react-redux'

export class ShipmentInfoCard extends Component {
   
    first_capital(string) 
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    render() {
        const shipment = this.props.shipment    
        return (
          <div>
               {shipment!=null ?(
                <div className=" pt-3 p-3 d-flex flex-column">
                    <div className="container">
                      <div className="float-left">
                      <div className='d-block'>
                        <span className="offer-description ">ID:</span>
                        <span className="offer-notes ml-2 ">{shipment._id}</span>
                        </div>
                      <div className='d-block'>
                        <span className="offer-description ">Description:</span>
                        <span className="offer-notes ml-2 ">{shipment.shipment_description}</span>
                        </div>
                        <div className='d-block'>
                        <img
                        src={require('./../Pics/Oval.png')}
                        alt="icon"
                        style={{ width: "11px", height: "11px" }}
                        className="img-fluid"
                      />
                        <span className="offer-description ml-2 mt-2">Waiting</span>
                      </div>
                        <div className='d-block'>
                        <span className="offer-notes ">Notes :</span>
                        <span className="offer-notes ml-2 ">..........</span>
                        </div>
                      </div>

                      <div className="float-right">
                        <span
                          className="d-block"
                          style={{ color: "#2e5bff", fontSize: "30px" }}
                        >
                          {shipment.price}$
                        </span>
                        <span
                          className="d-block text-muted ml-4"
                          style={{ fontSize: "18px" }}
                        >
                          {shipment.shipment_type}
                        </span>
                      </div>
                    </div>
                    <hr />
                    <div className="d-flex flex-column col-8">
                      <div className='d-flex flex-row justify-content-between'>
                        <div className='row'>
                        <p className="offer-description">Customer</p>
                        <p  className="offer-notes ml-2">Shipper LTL</p>
                        </div>
                        <p className="offer-notes ml-2">Axarnon,200,Greece</p>
                       </div>
                       <div className='row'>
                        <p className="offer-description">Name</p>
                        <p className="offer-notes ml-2">Dimitris Aristeidopoulos</p>
                       </div>
                       <div className='d-flex flex-row justify-content-between'>
                        <div className='row'>
                        <p className="offer-description">Tel.</p>
                        <p className="offer-notes ml-2">2104534546</p>
                        </div>
                        <div className='row'>
                        <p className="offer-description">Mob.</p>
                        <p className="offer-notes ml-2">6978111323</p>
                        </div>
                       </div>
                       
                    </div>                  
                    <hr />
                    <div className=" col-12 row"> 
                    <div className="col-6">
                      <img
                        src={require('./../Pics/arrowdown.png')}
                        alt="icon"
                        style={{ width: "30px", height: "30px" }}
                        className="img-fluid d-block"
                      />
                      <div className='d-flex justify-content-between'>
                      <span className="offer-description d-block mt-1">
                        {shipment.pickup_location.address}
                      </span>
                      <span className="offer-notes mt-1 ">122-453</span>
                      </div>
                     <span className="offer-notes d-block">
                        23/0/2020
                      </span> 
                      <span className="offer-description">Name</span>
                      <span className="offer-notes ml-2">
                       Dimitris Aristeidopoulos
                      </span>
                      <div>
                        <div className='d-flex flex-row justify-content-between'>
                        <div className="d-flex ">
                        <span className="offer-description">Tel.</span>
                        <span className="offer-notes ml-2">
                          6978111323
                        </span>
                        </div>
                        <div className="d-flex ">
                        <span className="offer-description">Mob.</span>
                        <span className="offer-notes ml-2">
                         6978111323
                        </span>
                      </div>
                      </div>
                      </div>
                    </div>

                      
                    <div className="col-6">
                      <img
                        src={require('./../Pics/arrowup.png')}
                        alt="icon"
                        style={{ width: "30px", height: "30px" }}
                        className="img-fluid d-block"
                      />
                      <div className='d-flex justify-content-between'>
                      <span className="offer-description d-block mt-1">
                        {shipment.dropoff_location.address}
                      </span>
                      <span className="offer-notes mt-1 ">122-453</span>
                      </div>
                     <span className="offer-notes d-block">
                        23/0/2020
                      </span> 
                      <span className="offer-description">Name</span>
                      <span className="offer-notes ml-2">
                       Dimitris Aristeidopoulos
                      </span>
                      <div>
                        <div className='d-flex flex-row justify-content-between'>
                        <div className="d-flex ">
                        <span className="offer-description">Tel.</span>
                        <span className="offer-notes ml-2">
                          6978111323
                        </span>
                        </div>
                        <div className="d-flex ">
                        <span className="offer-description">Mob.</span>
                        <span className="offer-notes ml-2">
                         6978111323
                        </span>
                      </div>
                      </div>
                      </div>
                    </div> 
                  </div>
                  <hr />
                  {/* delivery description */}
                  <div className="row">
                     {  Object.entries(shipment.shipment_attributes).map(item => {
                       if(item[0]!='_id'){
                      return (
                        <div key={item.weight} className="col-sm-2 mt-2">
                          <span className="offer-description d-block">
                            {this.first_capital(item[0])}
                          </span>
                          <span className="offer-notes">{item[1]==true?'Yes': item[1]==false ? 'No':item[1]}</span>
                        </div>
                      );}
                    })}
                  </div>
                  <hr />
                  {/* comment box */}
                  <div className="form-group mt-4">
                    <textarea
                      className="form-control"
                      rows="5"
                      id="comment"
                      style={{ border: " 0.5px dashed #777777" }}
                    ></textarea>
                    <span className="offer-notes d-block mt-4">
                      Notes :{shipment.notes==null? "":shipment.notes}
                    </span>
                  </div>
                </div>
          ):<h4 style={{margin:'auto'}}>Loading</h4>}  
          </div> 
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipmentInfoCard)
