import React from "react";
import "../Styles/NavBar.css";
import Images from "../Images";
import { NavLink } from "react-router-dom";
const Tabs = [
  { id: 1, text: "Shipments" },
  { id: 2, text: "Customers" },
  { id: 3, text: "Carriers" }
];
const Icon = [
  { id: 1, icon: <img source={require('./../Pics/bell.png')}></img>},
  { id: 2, icon: <i className="fa fa-envelope-o fa-lg" aria-hidden="true"></i> }
];

export default class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notification_popup:'none'
    };
  }
  toggle_notification=()=>{
    if(this.state.notification_popup=='flex'){
      this.setState({notification_popup:'none'})
    }else{
      this.setState({notification_popup:'flex'}) 
    }
  }
 render(){
  return (
    <div className="d-flex justify-content-md-around justify-content-between bg_white">
      {/* Logo */}
      <div className="d-flex align-item-center">
        <img src={Images.logo_with_text} alt="logo" />
      </div>
      {/* Tabs */}
      <div className="d-flex w-50">
      <ul className="navbar-navn">
        {Tabs.map(item => {
          return (
            <li>
            <NavLink
              to={`/${item.text}`}
              key={item.id}
              activeClassName="active"
            //  className='nav--item'
            >
              {item.text}
            </NavLink>
            </li>
          );
        })}
        </ul>
      </div>
      {/* icons */}
     
        {/* profile pic */}
        <div>
        <button onClick={this.toggle_notification}  style={{backgroundColor: 'transparent',borderWidth:0,outlineWidth:0}}><img src={require('./../Pics/bell.png')}style={{ width: "27px", height: "27px",marginRight:20 }} ></img></button>
         
         
          <img
            src={Images.profile_pic}
            alt="userpic"
            style={{ width: "45px", height: "45px" }}
          />
        </div>
        <div id='notification_modal' style={{display:this.state.notification_popup}}>
          <h4>Notification1</h4>
         </div>
      </div>
   
  );
      }
}
