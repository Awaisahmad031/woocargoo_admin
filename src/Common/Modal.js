import React, { Component } from 'react'
import '../Styles/Modal.css'
const Modal = ({ handleClose, show, children }) => {
    return (
      <div style={{zIndex:1}} className={show ? "modal display-block" : "modal display-none"}>
        <section className="modal-main">
          <button  style={{position:'fixed',top:10,right:10,background:'transparent',border:0}}
          onClick={handleClose}>
            <img src={require('./../Pics/close.png')}></img>
          </button>
          {children}
        </section>
      </div>
    );
  };
  export default Modal;