import React, { Component } from "react";
import "../Styles/Card.css";
import Images from "../Images";
import Progressbar from "./Progressbar";
import {Link}from 'react-router-dom'
export default class Card extends Component {
  render() {
    const {
      _id,
      shipment_status,
      progressvalue,
      pickup_location,
      dropoff_location,
      price
    } = this.props.item;

    return (
      <div className="container bgcolor mb-3 shadow-sm mt-2">
        <div className="row py-3">
          <div className="col-2">
            <div className="ID">
              <p className="numbercolor">{_id} </p>
              <p className="blueandbold">{this.props.item.shipment_description}</p>
              <p className="">{shipment_status}</p>
              {shipment_status=='Upcoming'? <Progressbar barvalue={progressvalue} /> :null}
            </div>
          </div>
          <div className="col-4">
            <img
              src={Images.pickupicon}
              className="img-fluid d-inline"
              alt="arrowup"
              style={{ width: "30px", height: "30px" }}
            ></img>
            <span className="blueandbold text-center addresssize ">
              &nbsp; {pickup_location.address}
            </span>
            <br />
            <br />
            <p className="startingdate ml-4 ">21 Jan</p>

            <p className="time numbercolor light_gray_text ml-4">9:30</p>
          </div>
          <div className="col-4">
            <img
              src={Images.deliveryicon}
              className="img-fluid d-inline"
              alt="arrowdown"
              style={{ width: "30px", height: "30px" }}
            ></img>
            <span className="blueandbold addresssize ">
              &nbsp; {dropoff_location.address}
            </span>
            <br />
            <br />
            <p className="startingdate ml-4">21 Jan</p>

            <p className="time numbercolor light_gray_text ml-4">9:30</p>
          </div>
          <div className="col-2">
            {shipment_status!='WAITING'?<h4 className="blue_text mt-5 ">{price}$</h4> :<h4 className="blue_text mt-5 ">Offer</h4> }
            
           <Link to={`/shipmentinfo/${_id}`}>
            <button className="btn btnsize px-2">Open</button>
           </Link>
          </div>
        </div>
      </div>
    );
  }
  
}
