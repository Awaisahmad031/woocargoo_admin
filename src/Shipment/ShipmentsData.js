export const ShipmentsData = [
  {
    id: 122,
    shipment_state: "Waiting",
    progressvalue: 20,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  },
  {
    id: 123,
    shipment_state: "In progress",
    progressvalue: 80,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  },
  {
    id: 124,
    shipment_state: "Upcoming",
    progressvalue: 50,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  },
  {
    id: 125,
    shipment_state: "Finished",
    progressvalue: 120,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  },
  {
    id: 126,
    shipment_state: "Waiting",
    progressvalue: 20,
    pickuplocation: "G. Lampraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  }
];
