import React, { Component } from 'react'
import Modal from './../../Common/Modal'



class SplitModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            later:null,
            show_later:true
        }
    }
     
    render() {
        return (
            
        <Modal handleClose={this.props.handleClose} show={this.props.visible} children={()=>this.splitmodal()}>
             {this.state.later==null?
             <div className='d-flex flex-row justify-content-center h-100 align-self-center'>
             <button onClick={()=>this.setState({later:true})} className='btn_blue m-2'>Later</button>
             <button onClick={()=>this.setState({later:false})}  className='btn_blue m-2'>Now</button>
             </div> :null
             } 
              
              {this.state.later ?(
                  <div>
                 <p className='gray_text text-uppercase m-2'>Split Shipment</p> 
                 <p className='m-2'>Use this page to Split Shipment</p> 
                 <div className='d-flex flex-column' >
                      <div className='d-flex flex-row' >
                     <div className='m-2'>
                          <label className='gray_text text-uppercase'>Carrier 1</label>
                          <input className='form-control' value=''></input>
                      </div>
                      <div className='m-2'>
                          <label className='gray_text text-uppercase'>Warehouse</label>
                          <input className='form-control' value=''></input>
                      </div>
                     </div>
                      <div className='d-flex flex-row' >
                      <div className='m-2'>
                          <label className='gray_text text-uppercase'>Carrier 2</label>
                          <input className='form-control' value=''></input>
                      </div>
                      <div className='m-2'>
                          <label className='gray_text text-uppercase'>Warehouse</label>
                          <input className='form-control' value=''></input>
                      </div>
                      <div className='m-2'>
                          <label className='gray_text text-uppercase'>Pickup Date</label>
                          <input className='form-control' value=''></input>
                      </div>
                      <div className='m-2'>
                          <label className='gray_text text-uppercase'>Pickup Time</label>
                          <input className='form-control' value=''></input>
                      </div>
                     </div>
                 </div> 
                 <button className='btn_blue ml-2 mt-4'>Split</button>
                 </div>
              )
                 :null
              }
              
                   
        </Modal>
     )
    }
}
export default SplitModal;
