import React, { Component } from "react";
import "../Styles/FilterModule.css";
export default class FilterModule extends Component {
  render() {
    return (
      <React.Fragment>
        <button className="button button_active" onClick={this.props.onClick}>
          {this.props.buttonText}
        </button>
      </React.Fragment>
    );
  }
}
