import Images from "../Images";
const CustomersData = [
  {
    id: 1,
    name: "Name",
    pic: Images.profile_pic,
    mobile: "690000000",
    address: "Kifisia,UK",
    occupation: "Paper Trade"
  },
  {
    id: 2,
    name: "Name",
    pic: Images.profile_pic,
    mobile: "690000000",
    address: "Kifisia,Athens",
    occupation: "Paper Trade"
  },
  {
    id: 3,
    name: "Name",
    pic: Images.profile_pic,
    mobile: "690000000",
    address: "Kifisia,Athens",
    occupation: "Paper Trade"
  },
  {
    id: 4,
    name: "Name",
    pic: Images.profile_pic,
    mobile: "690000000",
    address: "Kifisia,Athens",
    occupation: "Paper Trade"
  },
  {
    id: 5,
    name: "Name",
    pic: Images.profile_pic,
    mobile: "690000000",
    address: "Kifisia,Athens",
    occupation: "Paper Trade"
  }
];
export default CustomersData;
