import React from "react";
import "../Styles/Customer.css";
export default function InputField({
  label,
  label_text,
  name,
  changeHandler,
  value,
  className,
  type
}) {
  return (
    <div className={`form-group mb-4 ${className}`}>
      {label && <label className="filter-label">{label_text}</label>}
      <input
        type={type}
        name={`${name}`}
        className={`form-control`}
        onChange={changeHandler}
        placeholder=''
        value={value}
      />
    </div>
  );
}
