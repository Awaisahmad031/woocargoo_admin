import React, { Component } from "react";
import "../Styles/Customer.css";
import InputField from "./InputField";
export default class FilterModule extends Component {
  render() {
    const {
      changeHandler,
      title,
      state,
      searchRecord,
      Label_Info
    } = this.props;

    return (
      <div className="sizeofmodule shadow bg-white rounded">
        <form className="d-flex flex-column align-content-start">
          <div className="light_gray_text text-uppercase mb-4">{title}</div>
          {Label_Info.map(label=>{
           return <InputField
            name={label.field_name}
            label={label.label}
            label_text={label.label_text}
            changeHandler={changeHandler}
            value={state[label.field_name]}
            placeholder={label.placeholder}
          />
          })}
         
        </form>
        <button
          className="button btn btn-primary btn-block"
          onClick={searchRecord}
        >
          Search
        </button>
      </div>
    );
  }
}
