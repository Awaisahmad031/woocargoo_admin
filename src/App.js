import React from "react";
import "./Styles/bootstrap.css";
import "./Styles/bootstrap-grid.min.css";
import "./Styles/style.css";
import { Route, Switch, Redirect } from "react-router-dom";
import NavBar from "./Common/NavBar";
import Shipments from "./Pages/Shipments";
import Customers from "./Pages/Customers";
import Carriers from "./Pages/Carriers";
import ShipmentInfo from "./Pages/ShipmentInfo";

const RenderComp = [
  { id: 1, link: "/Shipments", Comp: Shipments },
  { id: 2, link: "/Carriers", Comp: Carriers  },
  { id: 3, link: "/Customers", Comp: Customers },
  { id: 4, link: "/shipmentinfo/:id", Comp: ShipmentInfo }
];
function App() {
  return (
    <React.Fragment>
      <NavBar />
      <Switch>
        {RenderComp.map(item => {
          return (
            <Route key={item.id} path={`${item.link}`} component={item.Comp}>
            </Route>
          );
        })}
        <Redirect from="/" to="/Shipments" />
      </Switch>
    </React.Fragment>
  );
}

export default App;
