import React, { Component } from "react";
import '../Styles/Shipments.css'
import ReactModal from 'react-modal';
import { connect } from 'react-redux'
import { get_shipment_by_id_async } from "../Redux/actions/shipment_actions_creators";
import { ShipmentInfoCard } from "../Common/ShipmentInfoCard";
import { get_carriers_async } from '../Redux/actions/carriers_actions_creators'
import socketIOClient from "socket.io-client";
import NoSplitModal from '../Common/NoSplitModal'
import SplitModal from '../Common/SplitModal'
import CustomerChatModal from '../Common/CustomerChatModal'
import OffersModal from '../Common/OffersModal'
class ShipmentInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modals_visibility: [
        false,//split
        false,//nosplit
        false,//offers
        false,//customer chat
      ],
      carriers_selected: [],
      split_modal_later: null,
      nosplit_modal_visibility: false,
      shipment_id: null,
      shipment: null
    };
  }
  
  componentDidMount() {

    this.props.get_carriers();
    this.setState({ shipment_id: this.props.match.params.id })
    this.props.get_shipment_by_id(this.props.match.params.id, this.get_shipment_by_id_callback)
  }
  get_shipment_by_id_callback = (success) => {
    if (success) {
      this.setState({ shipment: this.props.data.shipment_by_id })
    } else {
    }
  }
  handle_later_split = (later) => {
    console.log(later);

    if (later) {
      this.setState({ split_modal_later: true });
    } else {
      this.setState({ split_modal_later: false });
    }
  }
  handle_customer_chat = () => {
    console.log(this.state.shipment);

    const socket = socketIOClient('http://127.0.0.1:8000/admincarrierchat');
    socket.on('connect', () => {
      console.log('Connected to admin chat');
      socket.emit('joinchatroom', this.state.shipment._id);
      socket.emit('sendchatmessage', { shipmentid: this.state.shipment._id, msg_content: 'fefefef' });
    })
    console.log(socket);
  }
  setSelected = e => {
    console.log(e);
    this.setState({carriers_selected:e});
  };

  handle_modal_close = i => {
    if (i == 0) {
      this.setState({ split_modal_later: null })
    }
    if (i == 1) {

    }
    if (i == 3) {
      this.handle_customer_chat();
    }
    let a = this.state.modals_visibility.slice();
    a[i] = !a[i];
    this.setState({ modals_visibility: a })
  };
  checkUpComing = text => {
    if (text === "Upcoming" || text === "Finished") {
      return true
    }
  };
  render() {
    const shipment = this.state.shipment;
    
    return (
      <React.Fragment>
        <SplitModal
          isOpen={this.state.modals_visibility[0]}
          onClose={() => this.handle_modal_close(0)}
          handleSplit={this.handle_later_split}
          split_modal_later={this.state.split_modal_later}
        />
        <NoSplitModal
          isOpen={this.state.modals_visibility[1]}
          close={this.handle_modal_close}
          carriers={this.props.carriers_data.carriers}
          preselect_carriers={shipment!=null?shipment.carriers_selected_to_offer:[]}
          shipment_id={this.state.shipment_id}
        />
        <OffersModal
          isOpen={this.state.modals_visibility[2]}
          close={this.handle_modal_close}
          carriers={this.props.carriers_data.carriers}
          preselect_carriers={shipment!=null?shipment.carriers_selected_to_offer:[]}
          shipment_id={this.state.shipment_id}
        />
        <CustomerChatModal
          isOpen={this.state.modals_visibility[3]}
          onClose={() => this.handle_modal_close(3)}
        />
        <div className="col-6 mb-5 mt-2 " style={{ maxHeight: '90vh' }} >
          <div className="container shadow-sm" style={{ background: "#ffffff", maxHeight: '70%' }}>
            <div className="p-2 pt-4" style={{ maxHeight: '90vh', overflow: 'scroll' }}>
              <div className='d-flex flex-row justify-content-between'>
                <span className="text-uppercase " style={{ color: '#8798AD' }}>Shipment Information</span>
                <div className='d-flex flex-row'>
                  <button className='btn ml-3' onClick={() => this.handle_modal_close(2)} style={{ color: 'white', background: '#F7C137' }} >Offers</button>
                  <button className='btn ml-3' onClick={() => this.handle_modal_close(1)} style={{ color: 'white', background: '#00C1D4' }}>No Split</button>
                  <button className='btn ml-3' onClick={() => this.handle_modal_close(0)} style={{ color: 'white', background: '#8C54FF' }}>SplitShipment</button>
                  <button className='btn ml-3' onClick={() => this.handle_modal_close(3)} style={{ color: 'white', background: '#2E5BFF' }}>Customer Chat</button>
                  <button className='btn ml-3' style={{ color: 'white', background: '#D63649' }}>Edit</button>
                </div>
              </div>
              <hr />
              <div className="container   mt-3" style={{ height: '90%' }}>
               {shipment!=null ? <ShipmentInfoCard key={shipment._id} shipment={shipment} />:null}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    data: state.shipments_reducer,
    carriers_data: state.carriers_reducer,
  }
}
const mapDispatchToProps = dispatch => {
  return {
    get_carriers: () => dispatch(get_carriers_async()),
    get_shipment_by_id: (id, callback) => dispatch(get_shipment_by_id_async(id, callback))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipmentInfo);