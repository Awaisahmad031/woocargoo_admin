import React, { Component } from "react";
import FilterModule from "../Customer/FilterModule";
import PlusButton from "../Common/PlusButton";
import TitleRow from "../Common/TitleRow";
import CustomersData from "../Customer/CustomersData";
import CardCC from "../Common/CardCC";
import {connect} from 'react-redux'
import {get_customers_async} from '../Redux/actions/customer_actions_creators'
import ReactModal from 'react-modal';
import InputField from '../Customer/InputField'
import {customer_input_field, filter} from '../input_fields_data'
const Label_Info = [
  { label_text: "Customer ID", label: true, field_name: "_id",placeholder:'Customer ID' },
  { label_text: "Name", label: true, field_name: "customer_name" },
  { label_text: "Customer Email", label: true, field_name: "customer_email",placeholder:'Customer Email' },
  { label_text: "Mobile Number", label: true, field_name: "customer_mobilenum" }
];
class Customers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstfield: "",
      secondfield: "",
      address: "",
      type: "",
      data: CustomersData,
      filterrecord: [],
      allshow: true,
      filtershow: false,
      add_modal_visibility:false,
      edit_modal_visibility:false,
      selected_customer_for_edit:null
    };
  }
  changeHandler = e => {
    this.setState({ [e.currentTarget.name]: e.currentTarget.value });
  };
componentDidMount(){
  this.props.get_customers();
}

  //after clicking the search button..find record base on address
  searchRecord = () => {
   this.setState(filter(Label_Info,this.props.data.customers,this.state)); 
  };
  render() {
    console.log(this.state);
    
    return (
      <div className="container-fluid pt-5">
    <ReactModal
        id='split_modal'
        isOpen={this.state.add_modal_visibility}
        onRequestClose={() => this.setState({add_modal_visibility:false})}
        shouldCloseOnEsc={true}
        className='split_modal'
        overlayClassName='split_modal_overlay'
        shouldCloseOnOverlayClick={true}
      >
        <button  style={{position:'fixed',top:10,right:10,background:'transparent',border:0}}
        onClick={()=>this.setState({add_modal_visibility:false})}>
          <img src={require('./../Pics/close.png')}></img>
        </button>
        <h4 className='mt-3' style={{alignSelf:'center'}}>Add Customer</h4>
           <div className='d-flex flex-wrap flex-row col-12 mt-4'> 
           {
           customer_input_field.map(element => {
            return   <InputField  name={Label_Info[0].field_name}
              label='label'
              className='col-6'
              label_text={element.label}
              type={element.type}
             />  
           })
          }
            </div>  
            <button className='btn btn-primary col-3 ml-4'>Create</button>
      </ReactModal>

      <ReactModal
        id='split_modal'
        isOpen={this.state.edit_modal_visibility}
        onRequestClose={() => this.setState({edit_modal_visibility:false})}
        shouldCloseOnEsc={true}
        className='split_modal'
        overlayClassName='split_modal_overlay'
        shouldCloseOnOverlayClick={true}
      >
        <button  style={{position:'fixed',top:10,right:10,background:'transparent',border:0}}
        onClick={()=>this.setState({edit_modal_visibility:false})}>
          <img src={require('./../Pics/close.png')}></img>
        </button>
        <h4 className='mt-3' style={{alignSelf:'center'}}>Edit Customer</h4>
           <div className='d-flex flex-wrap flex-row col-12 mt-4'> 
           {this.state.selected_customer_for_edit!=null?
           customer_input_field.map(element => {
            return   <InputField  name={element.name}
            changeHandler={this.changeHandler}
              label='label'
              className='col-6'
              value={this.state.selected_customer_for_edit[element.name]}
              label_text={element.label}
              type={element.type}
             />  
           }):null
          }
            </div>  
            <button className='btn btn-primary col-3 ml-4'>Create</button>
      </ReactModal>

     


        <div className="row">
          {/* heading of card start*/}
          <div className="col-sm-8 offset-sm-1">
            <span className="main_title">Customers</span>
            <span className="total_numbers ml-3">
              {this.state.allshow && this.state.data.length}
              {this.state.filtershow && this.state.filterrecord.length} Total
            </span>
            {/*--------------title start---------- */}
            <TitleRow change1="Address" change2="Actions" />
            <div
              style={{
                overflow: "scroll",
                maxHeight: "70vh",
                msOverflowStyle: "none"
              }}
            >
              {/* all data show */}
              {this.state.allshow?
                this.props.data.customers.map(item => {
                  return <CardCC handleEdit={()=>this.setState({selected_customer_for_edit:item,edit_modal_visibility:true})} key={item._id} id={item._id} name={item.customer_name} email={item.customer_email} mobilenum={item.customer_mobilenum} />;
                }):null}
              {this.state.filtershow &&
                this.state.filterrecord.map(item => {
                  return <CardCC handleEdit={()=>this.setState({selected_customer_for_edit:item,edit_modal_visibility:true})} key={item._id} id={item._id} name={item.customer_name} email={item.customer_email} mobilenum={item.customer_mobilenum} />;
                })}

              {/* filter data */}
            </div>
          </div>
          <div className="col-sm-3">
            <FilterModule
              title="Filter Customers"
              state={this.state}
              Label_Info={Label_Info}
              changeHandler={this.changeHandler}
              searchRecord={this.searchRecord}
            />
            <div>
              <PlusButton onClick={()=>this.setState({add_modal_visibility:true})} />
            </div>
          </div>
          {/* plus button */}
        </div>
      </div>
    );
  }
}

const mapStateToProps=(state)=>{
  return{
    data:state.customers_reducer
  }
}
const mapDispatchToProps =dispatch=>{
  return {
    get_customers:()=>dispatch(get_customers_async())
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Customers);