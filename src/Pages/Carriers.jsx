import React, { Component } from "react";
import CarriersData from "../Carrier/CarriersData";
import CardCC from "../Common/CardCC";
import PlusButton from "../Common/PlusButton";
import TitleRow from "../Common/TitleRow";
import FilterModule from "../Customer/FilterModule";
import {connect} from 'react-redux'
import {get_carriers_async, add_carrier_async} from '../Redux/actions/carriers_actions_creators'

import ReactModal from 'react-modal';
import InputField from '../Customer/InputField'
import {carrier_add_input_field,filter} from '../input_fields_data'

const Label_Info = [
  { label_text: "Carrier ID", label: true, field_name: "_id" },
  { label_text: "Carrier Email", label: true, field_name: "carrier_email" },
  { label_text: "Address", label: true, field_name: "carrier_phone" },
  { label_text: "Type of Truck", label: true, field_name: "carrier_name" }
];


 class Carriers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstfield: "",
      secondfield: "",
      address: "",
      type: "",
      data: CarriersData,
      filterrecord: [],
      allshow: true,
      filtershow: false,
      add_modal_visibility:false,
      edit_modal_visibility:false,
      selected_carrier_for_edit:null
    };
  }
  componentDidMount(){
    this.props.get_carriers();
  }
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  add_carrier=()=>{
   const  body={
    carrier_name:this.state.carrier_name,
      carrier_email:this.state.carrier_email,
      carrier_phone:this.state.carrier_phone,
      carrier_password:this.state.carrier_password
    }
    this.props.add_carrier(body)
  }
  //after clicking the search button..find record just base on address
  searchRecord = () => {
    this.setState(filter(Label_Info,this.props.data.carriers,this.state)); 
   };
  render() {
    console.log(this.state);
  
    return (
      <div className="container-fluid pt-5">

<ReactModal
        id='split_modal'
        isOpen={this.state.add_modal_visibility}
        onRequestClose={() => this.setState({add_modal_visibility:false})}
        shouldCloseOnEsc={true}
        className='split_modal'
        overlayClassName='split_modal_overlay'
        shouldCloseOnOverlayClick={true}
      >
        <button  style={{position:'fixed',top:10,right:10,background:'transparent',border:0}}
        onClick={()=>this.setState({add_modal_visibility:false})}>
          <img src={require('./../Pics/close.png')}></img>
        </button>
        <h4 className='mt-3' style={{alignSelf:'center'}}>Add Carriers</h4>
           <div className='d-flex flex-wrap flex-row col-12 mt-4'> 
           {
           carrier_add_input_field.map(element => {
            return   <InputField  name={element.name}
            changeHandler={this.changeHandler}
              label='label'
              className='col-6'
              label_text={element.label}
              type={element.type}
             />  
           }) 
          }
            </div>  
            <button onClick={this.add_carrier} className='btn btn-primary col-3 ml-4'>Create</button>
      </ReactModal>


      <ReactModal
        id='split_modal'
        isOpen={this.state.edit_modal_visibility}
        onRequestClose={() => this.setState({edit_modal_visibility:false})}
        shouldCloseOnEsc={true}
        className='split_modal'
        overlayClassName='split_modal_overlay'
        shouldCloseOnOverlayClick={true}
      >
        <button  style={{position:'fixed',top:10,right:10,background:'transparent',border:0}}
        onClick={()=>this.setState({edit_modal_visibility:false})}>
          <img src={require('./../Pics/close.png')}></img>
        </button>
        <h4 className='mt-3' style={{alignSelf:'center'}}>Edit Carriers</h4>
           <div className='d-flex flex-wrap flex-row col-12 mt-4'> 
           {this.state.selected_carrier_for_edit!=null ?
            carrier_add_input_field.map(element => {
            return   <InputField  name={element.name}
            changeHandler={this.changeHandler}
              label='label'
              className='col-6'
              value={this.state.selected_carrier_for_edit[element.name]}
              label_text={element.label}
              type={element.type}
             />  
           }) :null
          }
            </div>  
            <button onClick={this.add_carrier} className='btn btn-primary col-3 ml-4'>Create</button>
      </ReactModal>


        <div className="row">
          {/* heading of card start*/}

          <div className="col-md-8 offset-md-1">
            <span className="main_title">Carriers</span>
            <span className="total_numbers ml-3">
              {this.state.allshow && this.state.data.length}
              {this.state.filtershow && this.state.filterrecord.length} Total
            </span>
            <TitleRow change1="Telephone" change2="Address" />
            <div
              style={{
                maxHeight: "80vh",
              }}
              className='hideoverflow'
            >
              {/* all data show */}
              {this.state.allshow &&
                this.props.data.carriers.map(item => {
                  return <CardCC handleEdit={()=>this.setState({selected_carrier_for_edit:item,edit_modal_visibility:true})} key={item._id} id={item._id} name={item.carrier_name} email={item.carrier_email} mobilenum={item.carrier_phone} />;
                })}
              {/* filter data */}
              {this.state.filtershow &&
                this.state.filterrecord.map(item => {
                  return <CardCC handleEdit={()=>this.setState({selected_carrier_for_edit:item,edit_modal_visibility:true})} key={item._id} id={item._id} name={item.carrier_name} email={item.carrier_email} mobilenum={item.carrier_phone} />;
                })}
            </div>
          </div>
          <div className="col-md-3">
            <FilterModule
              title="Carrier Filter"
              state={this.state}
              Label_Info={Label_Info}
              changeHandler={this.changeHandler}
              searchRecord={this.searchRecord}
            />
            <PlusButton onClick={()=>this.setState({add_modal_visibility:true})} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps=(state)=>{
  return{
    data:state.carriers_reducer
  }
}
const mapDispatchToProps =dispatch=>{
  return {
    get_carriers:()=>dispatch(get_carriers_async()),
    add_carrier:body=>dispatch((add_carrier_async(body)))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Carriers);