import React, { Component } from "react";
import { ShipmentsData } from "../Shipment/ShipmentsData";
import CardTitle from "../Shipment/CardTitle";
import Card from "../Common/Card";
import PlusButton from "../Common/PlusButton";
import FilterModule from "../Shipment/FilterModule";
import { connect } from 'react-redux'
import { get_shipments_async } from "../Redux/actions/shipment_actions_creators";

const buttontext = [
  { id: 1, shipment_state: "In progress" },
  { id: 2, shipment_state: "Upcoming" },
  { id: 3, shipment_state: "Finished" },
  { id: 4, shipment_state: "Waiting" }
];
const filter_status=['WAITING','UPCOMING','INPROGRESS']
 class Shipments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: ShipmentsData,
      newRecord: [],
      filter:'ALL'
    };
  }
 componentDidMount(){

  this.props.get_shipments([{
    id: 122,
    text: "Waiting",
    progressvalue: 20,
    pickuplocation: "G. Ldadmpraki 64, Piraeus",
    deliverylocation: " Kifisias A. 262, Marousi",
    price: 200
  }]);
  
 }
showAllShipments = ()=>{
this.setState({
      Allshipment: true,
      Filtershipment:false 
    });
}
  onPressButton = val => {
    val =String(val).toUpperCase()
    this.setState({
      filter:val  
    });
  };
  render() {
    console.log(this.props.shipments);
    
    return (
      <div className="container-fluid pt-5">
        <div className="row">
          {/* heading of card start*/}
          <div className="col-md-8 offset-md-1">
            <span className="main_title">Shipments</span>
            <span className="total_numbers ml-3">
             Total {this.props.data.shipments_count!=null ?this.props.data.shipments_count:null}
            </span>
            <CardTitle />
            <div
              style={{
                overflow: "scroll",
                maxHeight: "80vh",
                msOverflowStyle: "none",
                paddingBottom:40
              }}
              className='hiddenoverflow'
            >
              {this.props.data.loading&&this.props.data.shipments.length==0?<h4>Loading</h4>:null}
              {this.props.data.shipments.length==0?<h5>No Shipments found!</h5>:null}
s
              {/* all data show */}
              {this.props.data.show_all_shipments &&
                this.props.data.shipments.map(item => {
                  if(this.state.filter!='ALL'){
                    if(this.state.filter==item.shipment_status){
                      return <Card to='shipmentinfo'key={item.id} item={item} />;
                    }
                }else{
                    return <Card to='shipmentinfo'key={item.id} item={item} />;
                  }
                })}

              {/* filter data */}
              {this.props.show_filtered_shipments &&
                this.props.data.shipments.map(item => {
                  if(item.shipment_status== this.state)
                  return <Card key={item.id} item={item} />;
                })}
            </div>
          </div>
          <div className="col-md-3">
            <div
              className="sizeofmodule shadow-sm"
              style={{ background: "#ffffff" }}
            >
              <span
                style={{ alignSelf: "flex-start", marginBottom: 10 }}
                className="light_gray_text"
              >
                FILTER SHIPMENTS
              </span>

              <button className="button button_active"  onClick={() => this.onPressButton('all')} autoFocus>
                All Shipments
              </button>
              {buttontext.map(item => {
                return (
                  <FilterModule
                    key={item.id}
                    onClick={() => this.onPressButton(item.shipment_state)}
                    buttonText={item.shipment_state}
                  />
                );
              })}
            </div>
            <PlusButton />
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps=(state)=>{
  return{
    data:state.shipments_reducer

  }
}
const mapDispatchToProps =dispatch=>{
  return {
    get_shipments:(data)=>dispatch(get_shipments_async(data))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Shipments);