const Images = {
  logo_with_text: require("./Pics/logo_with_text.svg"),
  profile_pic: require("./Pics/profile_pic.png"),
  pickupicon: require("./Pics/arrowup.png"),
  deliveryicon: require("./Pics/arrowdown.png")
};

export default Images;
