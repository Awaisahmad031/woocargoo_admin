import { GET_CUSTOMERS, START_REQUEST } from "../constants";


const initialstate = {
  customers: [
  ],
  customers_count: null,
  loading:false,
  show_filtered_shipments:false,
  show_all_shipments:true
  };
  
  const customers_reducer = (state = initialstate, action) => {
    if (action.type === GET_CUSTOMERS) {
      return {
        ...state,
       customers:action.payload,
       customers_count:action.count,
       loading:false
      };
    }
    if (action.type === START_REQUEST) {
      return {
        ...state,
       loading:true
      };
    }
    return state;
  };
  
  export default customers_reducer;
  