import { GET_CARRIERS, START_REQUEST, ADD_CARRIER } from "../constants";


const initialstate = {
  carriers: [
  ],
  carriers_count: null,
  loading:false,
  //show_filtered_shipments:false,
 // show_all_shipments:true
  };
  
  const carriers_reducer = (state = initialstate, action) => {
    if (action.type === GET_CARRIERS) {
      return {
        ...state,
       carriers:action.payload,
       carriers_count:action.count,
       loading:false
      };
    }
    if (action.type === ADD_CARRIER) {
      console.log('form refucrer')
      const carriers = state.carriers.concat(action.payload)
      return {
        ...state,
       carriers 
    }
  }
    if (action.type === START_REQUEST) {
      return {
        ...state,
       loading:true
      };
    }
    return state;
  };
  
  export default carriers_reducer;
  