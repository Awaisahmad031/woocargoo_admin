import { GET_SHIPMENTS, START_REQUEST, GET_SHIPMENT_BY_ID, ADD_CARRIERS_TO_SHIPMENT } from "../constants";

const initialstate = {
  shipments: [
  ],
  shipments_count: null,
  shipment_by_id:null,
  loading:false,
  show_filtered_shipments:false,
  show_all_shipments:true
  };
  
  const shipmnets_reducer = (state = initialstate, action) => {
    if (action.type === GET_SHIPMENTS) {
      return {
        ...state,
       shipments:action.payload,
       shipments_count:action.count,
       loading:false
      };
    }
    if (action.type === GET_SHIPMENT_BY_ID) {
      let shipments = state.shipments; 
      shipments.push(action.payload)
      return {
        ...state,
        shipments,
        shipment_by_id:action.payload
      
      };
    }
    if (action.type === START_REQUEST) {
      return {
        ...state,
       loading:true
      };
    }
      if (action.type === ADD_CARRIERS_TO_SHIPMENT) {
        let shipments = state.shipments;
        shipments.map(shipment=>{
          if(shipment._id==action.id){
            shipment= action.shipment
          }
        })
        console.log(shipments);
        return {
          ...state,
          shipments
        };
        
      
    }
    return state;
  };
  
  export default shipmnets_reducer;
  