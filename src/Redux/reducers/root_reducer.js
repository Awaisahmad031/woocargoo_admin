import { combineReducers } from "redux";
import shipments_reducer from "./shipments_reducer";
import customers_reducer from "./customers_reducer";
import carriers_reducer from "./carriers_reducer";

const root_reducer = combineReducers({
    shipments_reducer,
    carriers_reducer,
    customers_reducer
});

export default root_reducer;
