import axios from 'axios';
import {URL} from '../constants'
import { get_carriers_action, start_request, add_carrier_action } from './carriers_actions';



export const get_carriers_async=()=>{
    return (dispatch)=>{
       // dispatch(start_request())
        axios
         .get(`${URL}admin/carriers/`)
         .then((response)=> {
            dispatch(get_carriers_action(response.data.data.allCarriers,null));
            console.log(response.data.data.allCarriers);
            
            //AddDriverSucess();
         })
         .catch(function(error) {
           if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
            console.log("Error", error.message);
            console.log("error 1");
           } else if (error.request) {
            console.log(error.request);
            } else {
             console.log("Error", error.message);
            }
             console.log(error.config);
              });
          };;
        

    }

    export const add_carrier_async=(body)=>{
      return (dispatch)=>{
         dispatch(start_request())
         console.log(body);
          axios
           .post(`${URL}admin/carriers/`,body)
           .then((response)=> {
             
              console.log(response.data.data.newcarrier);
              dispatch(add_carrier_action(response.data.data.newcarrier));
              //AddDriverSucess();
           })
           .catch(function(error) {
             if (error.response) {
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
              console.log("Error", error.message);
              console.log("error 1");
             } else if (error.request) {
              console.log(error.request);
              } else {
               console.log("Error", error.message);
              }
               console.log(error.config);
                });
            };;
          
  
      }
  