import {GET_SHIPMENTS,FAIL_REQUEST,START_REQUEST, GET_SHIPMENT_BY_ID, ADD_CARRIERS_TO_SHIPMENT} from '../constants'

export const get_shipment_action =(shipments,count)=>{
return {
    type: GET_SHIPMENTS,
    payload:shipments,
    count
}
}
export const start_request=()=>{
    
    return {
        type: START_REQUEST,
    }
}
export const fail_request=()=>{
    return {
        type: FAIL_REQUEST,
    }
}
export const get_shipment_by_id_action =(shipment)=>{
    return {
        type: GET_SHIPMENT_BY_ID,
        payload:shipment,
    }
}
export const add_carriers_to_shipment_action =(id,shipment)=>{
    return {
        type: ADD_CARRIERS_TO_SHIPMENT,
        id,
        shipment
    }
}


 