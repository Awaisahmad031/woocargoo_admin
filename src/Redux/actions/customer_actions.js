import {GET_CUSTOMERS,FAIL_REQUEST,START_REQUEST} from '../constants'

export const get_customers_action =(customers,count)=>{
return {
    type: GET_CUSTOMERS,
    payload:customers,
    count
}
}
export const start_request=()=>{
    
    return {
        type: START_REQUEST,
    }
}
export const fail_request=()=>{
    return {
        type: FAIL_REQUEST,
    }
}
 