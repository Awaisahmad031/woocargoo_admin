import {get_shipment_action,loading, start_request, get_shipment_by_id_action,add_carriers_to_shipment_action} from './shipment_actions'
import axios from 'axios';
import {URL} from '../constants'
export const get_shipments_async=()=>{
    return (dispatch)=>{
        dispatch(start_request())
        axios
         .get(`${URL}admin/shipments/`)
         .then((response)=> {
            dispatch(get_shipment_action(response.data.data.shipments,response.data.resultcount));
         //   console.log(response.data);
            
            //AddDriverSucess();
         })
         .catch(function(error) {
           if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
            console.log("Error", error.message);
            console.log("error 1");
           } else if (error.request) {
            console.log(error.request);
            } else {
             console.log("Error", error.message);
            }
             console.log(error.config);
              });
          };;
        

    }

    export const get_shipment_by_id_async=(id,callback)=>{
      return (dispatch)=>{
          dispatch(start_request())
          axios
           .get(`${URL}admin/shipments/${id}`)
           .then((response)=> {
              dispatch(get_shipment_by_id_action(response.data.data.shipment));
              callback(true)
              //AddDriverSucess();
           })
           .catch(function(error) {
             if (error.response) {
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
              console.log("Error", error.message);
              console.log("error 1");
             } else if (error.request) {
              console.log(error.request);
              } else {
               console.log("Error", error.message);
              }
               console.log(error.config);
                });
            };;
      }

      export const add_carriers_to_shipment=(id,body,callback)=>{
         return (dispatch)=>{
             dispatch(start_request())
             axios
              .post(`${URL}shipments/${id}/addcarriers`,body)
              .then((response)=> {
                 console.log(response.data.data.shipment)
                 dispatch(add_carriers_to_shipment_action(id,response.data.data.shipment));
                 callback(true);
              })
              .catch(function(error) {
                if (error.response) {
                  callback(false);
                 console.log(error.response.data);
                 console.log(error.response.status);
                 console.log(error.response.headers);
                 console.log("Error", error.message);
                 console.log("error 1");
                } else if (error.request) {
                 console.log(error.request);
                 } else {
                  console.log("Error", error.message);
                 }
                  console.log(error.config);
                   });
               };;
         }
  