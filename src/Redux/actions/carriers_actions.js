import {GET_CARRIERS,FAIL_REQUEST,START_REQUEST, ADD_CARRIER} from '../constants'

export const get_carriers_action =(carriers,count)=>{
return {
    type: GET_CARRIERS,
    payload:carriers,
    count
}
}
export const add_carrier_action =(carrier)=>{
    return {
        type: ADD_CARRIER,
        payload:carrier,
    }
    }

export const start_request=()=>{
    
    return {
        type: START_REQUEST,
    }
}
export const fail_request=()=>{
    return {
        type: FAIL_REQUEST,
    }
}
